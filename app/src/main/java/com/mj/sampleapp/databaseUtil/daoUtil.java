package com.mj.sampleapp.databaseUtil;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.mj.sampleapp.model.DataModel;
import java.util.List;

@Dao
public interface daoUtil {
    @Query("SELECT * FROM datamodel")
    List<DataModel> getAll();

    @Insert
    void insert(DataModel dataModel);

}
