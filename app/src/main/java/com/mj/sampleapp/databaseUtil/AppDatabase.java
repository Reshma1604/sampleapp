package com.mj.sampleapp.databaseUtil;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.mj.sampleapp.model.DataModel;

@Database(entities = {DataModel.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract daoUtil taskDao();

}
