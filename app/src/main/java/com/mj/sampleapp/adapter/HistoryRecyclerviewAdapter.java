package com.mj.sampleapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mj.sampleapp.R;
import com.mj.sampleapp.model.DataModel;
import java.util.List;

public class HistoryRecyclerviewAdapter extends RecyclerView.Adapter<HistoryRecyclerviewAdapter.HistoryViewHolder>{

    private Context mContext;
    private List<DataModel> mJobDetailsList;

    public HistoryRecyclerviewAdapter(Context mContext, List<DataModel> jobDetailsList) {
        this.mContext = mContext;
        this.mJobDetailsList = jobDetailsList;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.history_list_layout, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryViewHolder holder,final int position) {
        final DataModel dataModel = mJobDetailsList.get(position);
        holder.mTxtGameDateTime.setText("Game "+dataModel.getId()+" : "+dataModel.getDate_time());
        holder.mTxtUSerName.setText("Name : "+dataModel.getName());
        holder.mTxtQuestion2.setText(dataModel.getQuestion2());
        holder.mTxtAnswer2.setText("Answer : "+dataModel.getAnswer2());
        holder.mTxtQuestion3.setText(dataModel.getQuestion3());
        holder.mTxtAnswer3.setText("Answers : "+dataModel.getAnswer3());
    }

    @Override
    public int getItemCount() {
        return mJobDetailsList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder{
        AppCompatTextView mTxtGameDateTime;
        AppCompatTextView mTxtUSerName;
        AppCompatTextView mTxtQuestion2;
        AppCompatTextView mTxtAnswer2;
        AppCompatTextView mTxtQuestion3;
        AppCompatTextView mTxtAnswer3;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mTxtGameDateTime      = (AppCompatTextView) itemView.findViewById(R.id.txtGameNameTime);
            mTxtUSerName      = (AppCompatTextView) itemView.findViewById(R.id.txtName);
            mTxtQuestion2      = (AppCompatTextView) itemView.findViewById(R.id.txtQues2);
            mTxtAnswer2      = (AppCompatTextView) itemView.findViewById(R.id.txtAns2);
            mTxtQuestion3      = (AppCompatTextView) itemView.findViewById(R.id.txtQues3);
            mTxtAnswer3      = (AppCompatTextView) itemView.findViewById(R.id.txtAns3);
        }
    }

}
