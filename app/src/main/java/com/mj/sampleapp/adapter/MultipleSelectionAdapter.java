package com.mj.sampleapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.mj.sampleapp.R;
import com.mj.sampleapp.model.OptionModel;
import java.util.ArrayList;

public class MultipleSelectionAdapter extends RecyclerView.Adapter<MultipleSelectionAdapter.CheckboxViewHolder>{
    private Context mContext;
    private ArrayList<OptionModel> mOptionList;
    private MultipleSelectionAdapter.OnMultipleItemSelect mOnStandardPopupMultipleItemSelect;

    public MultipleSelectionAdapter(Context mContext, ArrayList<OptionModel> jobDetailsList) {
        this.mContext = mContext;
        this.mOptionList = jobDetailsList;
    }

    @NonNull
    @Override
    public MultipleSelectionAdapter.CheckboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.checkbox_list, parent, false);
        return new MultipleSelectionAdapter.CheckboxViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MultipleSelectionAdapter.CheckboxViewHolder holder, final int position) {
        final OptionModel optionModel = mOptionList.get(position);
        holder.mTxtTitle.setText(optionModel.getOption());
        if (optionModel.isSelected()){
            holder.mTxtTitle.setChecked(true);
        }else {
            holder.mTxtTitle.setChecked(false);
        }

        holder.mTxtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<mOptionList.size(); i++){
                    if (i==position){
                        if (mOptionList.get(position).isSelected()) {
                            mOptionList.get(position).setSelected(false);
                        } else {
                            mOptionList.get(position).setSelected(true);
                        }
                    }
                }
                if (mOnStandardPopupMultipleItemSelect != null) {
                    mOnStandardPopupMultipleItemSelect.onMultipleSelect(mOptionList);
                }
            }
        });
    }

    public interface OnMultipleItemSelect {
        void onMultipleSelect(ArrayList<OptionModel> list);
    }

    public void setOnMultipleItemSelect(MultipleSelectionAdapter.OnMultipleItemSelect onStandardPopupMultipleItemSelect) {
        this.mOnStandardPopupMultipleItemSelect = onStandardPopupMultipleItemSelect;
    }

    @Override
    public int getItemCount() {
        return mOptionList.size();
    }

    public class CheckboxViewHolder extends RecyclerView.ViewHolder{
        AppCompatCheckBox mTxtTitle;
        public CheckboxViewHolder(@NonNull View itemView) {
            super(itemView);
            mTxtTitle      = (AppCompatCheckBox) itemView.findViewById(R.id.txtCheckbox);
        }
    }
}
