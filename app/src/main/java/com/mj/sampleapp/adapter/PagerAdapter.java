package com.mj.sampleapp.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mj.sampleapp.R;
import com.mj.sampleapp.activity.HistoryActivity;
import com.mj.sampleapp.model.OptionModel;
import com.mj.sampleapp.model.PagerModel;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class PagerAdapter extends androidx.viewpager.widget.PagerAdapter implements MultipleSelectionAdapter.OnMultipleItemSelect{
    private Context mContext;
    private ArrayList<PagerModel> mList;
    private AppCompatTextView mTxtQuestion;
    private AppCompatEditText mEtUserName;
    private String name = "";
    private RecyclerView mRvCheckBox;
    private AppCompatRadioButton appCompatRadioButtons[] = null;
    private RadioGroup mLlRadio;
    private PagerModel modelObjectSelection;

    public PagerAdapter(Context context, ArrayList<PagerModel> pagerModelList) {
        mContext = context;
        mList = pagerModelList;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        final PagerModel modelObject = mList.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.pager_item_layout, collection, false);
        collection.addView(itemView);

        mTxtQuestion = (AppCompatTextView) itemView.findViewById(R.id.txtQuestion);
        mEtUserName = (AppCompatEditText) itemView.findViewById(R.id.etName);
        mRvCheckBox   = (RecyclerView) itemView.findViewById(R.id.recyclerView);
        mLlRadio                 = (RadioGroup) itemView.findViewById(R.id.llRadio);

        mTxtQuestion.setText(modelObject.getQuestion_name());
        mEtUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
            name = s.toString();
            }
        });
        mEtUserName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE ){
                    if (mFieldListener != null){
                        mFieldListener.addQuestionAnswer(modelObject.getQuestion_name(), name, modelObject.getQuestion_type());
                    }
                }
                return false;
            }
        });
        questionUI(modelObject.getQuestion_type(), modelObject);
        return itemView;
    }

    private void questionUI(int question_type,final PagerModel modelObject) {

        switch (question_type) {
            case 1:
                mEtUserName.setVisibility(View.VISIBLE);
                break;
            case 2:
                mLlRadio.setVisibility(View.VISIBLE);
                final ArrayList<OptionModel> mOptionList = modelObject.getmOption();
                appCompatRadioButtons = new AppCompatRadioButton[mOptionList.size()];
                int index = 0;
                for (int i = 0; i < mOptionList.size(); i++) {
                    index = i;
                    appCompatRadioButtons[i] = new AppCompatRadioButton(mContext);
                    appCompatRadioButtons[i].setText(mOptionList.get(i).getOption());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        appCompatRadioButtons[i].setTextColor(mContext.getColor(R.color._4F4F4F));
                    }
                    appCompatRadioButtons[i].setTextSize(18);
                    mLlRadio.addView(appCompatRadioButtons[i]);
                    if (mOptionList.get(i).isSelected()) {
                        appCompatRadioButtons[i].setChecked(true);
                    }
                    final int finalIndex = index;
                    appCompatRadioButtons[index].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mFieldListener != null) {
                                mFieldListener.addQuestionAnswer(modelObject.getQuestion_name(), mOptionList.get(finalIndex).getOption(), modelObject.getQuestion_type());
                            }
                        }
                    });
                }
                break;
            case 3:
                mRvCheckBox.setVisibility(View.VISIBLE);
                modelObjectSelection = modelObject;
                ArrayList<OptionModel> mOptionList2 = modelObject.getmOption();
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
                mRvCheckBox.setLayoutManager(layoutManager);
                MultipleSelectionAdapter selectionAdapter = new MultipleSelectionAdapter(mContext, mOptionList2);
                mRvCheckBox.setAdapter(selectionAdapter);
                selectionAdapter.setOnMultipleItemSelect(this);
                break;
        }
    }

    private PagerAdapter.getInputFieldListener mFieldListener;

    @Override
    public void onMultipleSelect(ArrayList<OptionModel> list) {
        StringBuilder optionBuilder = new StringBuilder();
        for (OptionModel optionModel : list){
          if (optionModel.isSelected()){
              optionBuilder.append(optionModel.getOption());
              optionBuilder.append(", ");
          }
      }
       String jobname = optionBuilder.toString();
       String finalStrList = jobname.substring(0, jobname.length() - ", ".length());
        if (mFieldListener != null){
          mFieldListener.addQuestionAnswer(modelObjectSelection.getQuestion_name(), finalStrList, modelObjectSelection.getQuestion_type());
      }
    }

public interface getInputFieldListener {
        void addQuestionAnswer(String question, String answer, int question_type);
    }
    public void saveFieldListener(PagerAdapter.getInputFieldListener fieldListener) {
        this.mFieldListener = fieldListener;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        PagerModel customPagerEnum = mList.get(position);
        return mContext.getString(customPagerEnum.getQuestion_name().length());
    }
}
