package com.mj.sampleapp.util;

import android.app.Activity;
import android.content.Intent;

import com.mj.sampleapp.activity.HistoryActivity;
import com.mj.sampleapp.activity.MainActivity;

public class IntentUtil {
    public static void openMainActivity(Activity activity){
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }
    public static void openHistoryActivity(Activity activity){
        Intent intent = new Intent(activity, HistoryActivity.class);
        activity.startActivity(intent);
    }
}
