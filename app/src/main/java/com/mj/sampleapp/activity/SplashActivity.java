package com.mj.sampleapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.mj.sampleapp.R;
import com.mj.sampleapp.util.IntentUtil;
import com.mj.sampleapp.util.StatusBarUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        StatusBarUtil.statusBarSetup(this);
        handler.postDelayed(
                new Runnable() {
                    public void run() {
                        finishSplash();
                    }
                }, 1500);
    }

    public void finishSplash(){
            IntentUtil.openMainActivity(this);
            finish();

    }

}
