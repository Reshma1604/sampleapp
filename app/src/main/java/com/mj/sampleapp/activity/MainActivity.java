package com.mj.sampleapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.mj.sampleapp.R;
import com.mj.sampleapp.adapter.PagerAdapter;
import com.mj.sampleapp.model.OptionModel;
import com.mj.sampleapp.model.PagerModel;
import com.mj.sampleapp.util.StatusBarUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements PagerAdapter.getInputFieldListener{
   private ViewPager mViewPager;
   private String mQuestion = "";
   private String mAnswer = "";
   private int mViewPagerPosition;
   private ArrayList<PagerModel> mSaveList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.statusBarSetup(this);
        setContentView(R.layout.activity_main);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        AppCompatButton mBtnNextButton = (AppCompatButton) findViewById(R.id.btnNextButton);

        ArrayList<PagerModel> mGameList = new ArrayList<>();
        PagerModel pagerModel1 = new PagerModel();
        pagerModel1.setQuestion_name("What is your name?");
        pagerModel1.setQuestion_type(1);
        mGameList.add(pagerModel1);

        PagerModel pagerModel2 = new PagerModel();
        pagerModel2.setQuestion_name("Who is the best cricketer in the world?");
        pagerModel2.setQuestion_type(2);
        ArrayList<OptionModel> optionList = new ArrayList<>();
        OptionModel optionModel1 = new OptionModel();
        optionModel1.setOption("Sachin Tendulkar");
        OptionModel optionModel2 = new OptionModel();
        optionModel2.setOption("Virat Kolli");
        OptionModel optionModel3 = new OptionModel();
        optionModel3.setOption("Adam Gilchirst");
        OptionModel optionModel4 = new OptionModel();
        optionModel4.setOption("Jacques Kallis");
        optionList.add(optionModel1);
        optionList.add(optionModel2);
        optionList.add(optionModel3);
        optionList.add(optionModel4);
        pagerModel2.setmOption(optionList);
        mGameList.add(pagerModel2);

        PagerModel pagerModel3 = new PagerModel();
        pagerModel3.setQuestion_name("What are the colors in the Indian national flag?");
        pagerModel3.setQuestion_type(3);
        ArrayList<OptionModel> optionSelectList = new ArrayList<>();
        OptionModel optionSelect1= new OptionModel();
        optionSelect1.setOption("White");
        optionSelect1.setSelected(false);
        OptionModel optionSelect2 = new OptionModel();
        optionSelect2.setOption("Yellow");
        optionSelect2.setSelected(false);
        OptionModel optionSelect3 = new OptionModel();
        optionSelect3.setOption("Orange");
        optionSelect3.setSelected(false);
        OptionModel optionSelect4 = new OptionModel();
        optionSelect4.setOption("Green");
        optionSelect4.setSelected(false);
        optionSelectList.add(optionSelect1);
        optionSelectList.add(optionSelect2);
        optionSelectList.add(optionSelect3);
        optionSelectList.add(optionSelect4);
        pagerModel3.setmOption(optionSelectList);
        mGameList.add(pagerModel3);

        final PagerAdapter mPagerAdapter = new PagerAdapter( this, mGameList);
        mViewPager.setAdapter(mPagerAdapter);
        mPagerAdapter.saveFieldListener(this);
        mViewPager.setOffscreenPageLimit(0);
        mViewPagerPosition = mGameList.size()-1;

        mBtnNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    private void saveData(){
                if (!mQuestion.isEmpty() & !mAnswer.isEmpty()) {
                    if (mViewPager.getCurrentItem() == mViewPagerPosition){
                        mQuestion = "";
                        mAnswer = "";
                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("dd MMM hh:mm a");
                        String strDate = dateFormat.format(date);
                        Intent intent = new Intent(MainActivity.this, SummaryActivity.class);
                        intent.putParcelableArrayListExtra("game",mSaveList);
                        intent.putExtra("date",strDate);
                        startActivity(intent);
                        finish();
                    }else {
                        mQuestion = "";
                        mAnswer = "";
                        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                    }
                }else {
                    Toast.makeText(MainActivity.this, "Please add the required filed", Toast.LENGTH_LONG).show();
                }
    }

    @Override
    public void addQuestionAnswer(String question, String answer, int qType) {
        mQuestion = question;
        mAnswer  = answer;
        PagerModel pagerModel = new PagerModel();
        pagerModel.setQuestion_name(mQuestion);
        pagerModel.setOption_name(mAnswer);
        mSaveList.add(pagerModel);
        if (mSaveList.size() > 3) {
            if (qType == 3) {
                if (question.equals(pagerModel.getQuestion_name())) {
                    mSaveList.remove(2);
                }
            }
        }
    }
}
