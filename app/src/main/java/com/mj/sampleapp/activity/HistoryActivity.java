package com.mj.sampleapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;

import com.mj.sampleapp.R;
import com.mj.sampleapp.adapter.HistoryRecyclerviewAdapter;
import com.mj.sampleapp.databaseUtil.DatabaseClient;
import com.mj.sampleapp.model.DataModel;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {
   private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistoryActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);
        getTasks();
    }

    private void getTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<DataModel>> {
            @Override
            protected List<DataModel> doInBackground(Void... voids) {
                List<DataModel> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<DataModel> tasks) {
                super.onPostExecute(tasks);
                HistoryRecyclerviewAdapter adapter = new HistoryRecyclerviewAdapter(HistoryActivity.this, tasks);
                mRecyclerView.setAdapter(adapter);
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }
}
