package com.mj.sampleapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import com.mj.sampleapp.R;
import com.mj.sampleapp.databaseUtil.DatabaseClient;
import com.mj.sampleapp.model.DataModel;
import com.mj.sampleapp.model.PagerModel;
import com.mj.sampleapp.util.IntentUtil;
import java.util.ArrayList;

public class SummaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        ArrayList<PagerModel> mQuestionList = getIntent().getParcelableArrayListExtra("game");
        String date = getIntent().getStringExtra("date");

        AppCompatTextView mTxtName = (AppCompatTextView) findViewById(R.id.txtUserName);
        AppCompatTextView mTxtQuestion1 = (AppCompatTextView) findViewById(R.id.txtQuestion1);
        AppCompatTextView mTxtAnswer1 = (AppCompatTextView) findViewById(R.id.txtAnswer1);
        AppCompatTextView mTxtQuestion2 = (AppCompatTextView) findViewById(R.id.txtQuestion2);
        AppCompatTextView mTxtAnswer2 = (AppCompatTextView) findViewById(R.id.txtAnswer2);
        AppCompatButton mBtnFinish = (AppCompatButton) findViewById(R.id.btnFinish);
        AppCompatButton mBtnHistory = (AppCompatButton) findViewById(R.id.btnHistory);

        mTxtName.setText("Hello "+ mQuestionList.get(0).getOption_name() +" : ,");
        mTxtQuestion1.setText(mQuestionList.get(1).getQuestion_name());
        mTxtAnswer1.setText("Answer : "+mQuestionList.get(1).getOption_name());
        mTxtQuestion2.setText(mQuestionList.get(2).getQuestion_name());
        mTxtAnswer2.setText("Answers : "+mQuestionList.get(2).getOption_name());

        mBtnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtil.openMainActivity(SummaryActivity.this);
                finish();
            }
        });

        mBtnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtil.openHistoryActivity(SummaryActivity.this);
            }
        });
        saveData(mQuestionList, date);
    }

    private void saveData(final ArrayList<PagerModel> mQuestionList, final String date){
        class SaveTask extends AsyncTask<Void, Void, Void> {
            DataModel dataModel = new DataModel();
            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                dataModel.setName(mQuestionList.get(0).getOption_name());
                dataModel.setQuestion2(mQuestionList.get(1).getQuestion_name());
                dataModel.setAnswer2(mQuestionList.get(1).getOption_name());
                dataModel.setQuestion3(mQuestionList.get(2).getQuestion_name());
                dataModel.setAnswer3(mQuestionList.get(2).getOption_name());
                dataModel.setDate_time(date);
                //adding to database
                DatabaseClient.getInstance(getApplication()).getAppDatabase()
                        .taskDao()
                        .insert(dataModel);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                        dataModel.setFinished(true);
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }

}
