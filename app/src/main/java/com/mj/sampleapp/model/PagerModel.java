package com.mj.sampleapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PagerModel implements Parcelable {
    private String question_name;
    private String option_name;
    private int question_type;

    protected PagerModel(Parcel in) {
        question_name = in.readString();
        option_name = in.readString();
        question_type = in.readInt();
    }

    public static final Creator<PagerModel> CREATOR = new Creator<PagerModel>() {
        @Override
        public PagerModel createFromParcel(Parcel in) {
            return new PagerModel(in);
        }

        @Override
        public PagerModel[] newArray(int size) {
            return new PagerModel[size];
        }
    };

    public PagerModel() {
    }

    public ArrayList<OptionModel> getmOption() {
        return mOption;
    }

    public void setmOption(ArrayList<OptionModel> mOption) {
        this.mOption = mOption;
    }

    ArrayList<OptionModel> mOption;

    public int getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(int question_type) {
        this.question_type = question_type;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question_name);
        dest.writeString(option_name);
        dest.writeInt(question_type);
    }
}
